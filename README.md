# Forged in Lies

Forged in Lies je rogue like hra, s puzzle prvky. Hlavní inspirací hry je již vzniklá
hra [Loot River](https://store.steampowered.com/app/1494260/Loot_River/)

## Tým

| Jméno            | Username | Hlavní role  |  Zabývané oblasti                   |
|------------------|----------|--------------|-------------------------------------|
| Martin Čáslavský | caslama1 | Vedoucí týmu | Game design, dialogový systém       |
| Martin Felkl     | felklmar | Grafika      | Design NPC, modelování scén         |
| Vítězslav Hušek  | husekvit | Příběh       | Game design, příběh                 |
| Ondřej Svějstil  | svejsond | Programování | Animace, úprava scén, optimalizace  |
| Jiří Šáda        | sadajiri | Programování | Základní funkcionality v UE         |
| Julie Plotnikova | plotnjul | Grafika      | Design prostředí                    |

## Dokumenty

* [Concept Art](https://gitlab.fit.cvut.cz/BI-VHS/b231_projects/forged_in_lies/-/tree/master/Doc/Concept%20Art?ref_type=heads)
* [Game Design](https://gitlab.fit.cvut.cz/BI-VHS/b231_projects/forged_in_lies/-/tree/master/Doc/Game%20design?ref_type=heads)
* [Static World](https://gitlab.fit.cvut.cz/BI-VHS/b231_projects/forged_in_lies/-/tree/master/Doc/Static%20World/pdf/static_world.pdf)

## Současný stav projektu

Sestavená hra se nachází na tomto odkazu: https://drive.google.com/drive/folders/1jgYocdFjKqZmF0wPKG76nc4rzCTPlJ-q?usp=sharing.

Hra má vytvořené 5 scén: 0_mainmenu, 1_lighthouse_inside, 2_factory_outside, 3_factory_inside a 4_safe_hub.

Scéna, která se zabývá hlavním menu má v sobě tři tlačítka (Start, Oprtions a Exit).
Při kliknutí na tlačítko start se spustí 1_lighthouse_inside.

V této scéně se hráč ocitne v pokoji majáku.
V místnosti se nachází postel, stůl, skříň a dummy.
Tu hráč musí rozbít, aby se mohl dostat do další scény skrz dveře.
Jednotlivé monologové proslovy, lze pomocí klavésy ENTER přeskočit.
Součástí této scény je i informace o tom, jak se může hráč pohybovat, případně i zaútočit.

V další scéně se hráč vyskytne na ostrově, kde je i daný maják.
Z ostrova se musí dostat a musí najít platformy.
Celou dobu vede postava sama se sebou monolog.
Po doražení k platformám získá hráč nový úkol: za pomocí platform se dostat pryč.
V monologu mu je vysvětleno, jak má platformy ovládat.
Pokud dorazí na druhý ostrov, najde před vchodem robota, s kterým může provést dialog.
Opět, pomocí monologového tutoriálu mu je řečeno, jak může do dialogu vstoupit.
Ten může (ale nemusí) vyústit v první souboj.
Ať už hráč s robotem bude bojovat, nebo ne, dostane se dveřmi do továrny.

V továrně má hráč jeden jediný úkol: dostat se z ní ven.
Továrna je plná robotů, kteří bez váhání na hráče zaútočí, včetně workera, který se stará o mini továrnu, která
vytváří nové a nové jednotky robotů.
Pokud se hráč dostane až ke dveřím, dojde k zjištění, že jsou zamčené.
Musí nejdříve vyřešit jednoduchou hádanku, která se nachází uprostřed mapy.
Poté se může dostat ven.

Poslední scéna je zatím pouze jako model, nestihla se dodělat funkcionalita.
Hráči je oznáměno, že se nachází na konci ukázku, hru tak může opustit.
Co se týká NPC, tak se jich pár v této scéně nachází.
Je zde prodejce a Anna, se kterémi lze vést originální dialog.
Ostatní NPC mají generický dialog v rámci safe hubu.

### Vlastní systémy
Pro naše potřeby vzniklo několik vlastních systémů, které lze nadálet
rozvíjet a upravovat pro naše potřeby hry.
Vývoj těchto systémů nám dal časově zabrat, ale urychlí případný
další vývoj projektu.
Mezi tyto systémy patří:

#### Combat systém
Combat systém je pro hry typu *rogue_like* zásadní.
My máme vytvořený velmi jednoduchý, abychom mohli ukázat, že se zde určitý
soubojový prvky nachází.
Každý NPC a některé předměty mají v sobě tzv. **BP_DamageSystemComponent**,
která se stará o všechno.
Vyhledává v okolí ostatní tyto komponenty a deleguje na ně svůj útok.
Ty reagují patřičným odebráním životů.
V případě, že dojde k poklesu životů pod 0, zahlasí komponenta, že je mrtvá.

#### Dialogový systém
Pro Unreal Engine existuje mnoho řešení dialogů, ale většina dobrých je zpoplatněna.
Proto jsme se rozhodli vytvořit dialogový systém vlastní.
I díky *blueprintů*, které se v UE hojně používají, je následné používání
velmi podobné vytváření dialogových stromů, což může zlehčit práci vývojářům.
Pokud se však strom hodně větví, může to být velmi nepřehledné.
I tak je to pro nás výhoda, lze na tento systém případně nabalovat nějaké úkoly,
či aktualizovat jejich progres.

#### Systém platforem a lokací
Jednou z hlavních mechanik v naší hře jsou různé hádanky, které bude potřeba
řešit za pomocí platforem.
V naší ukázce se jedna taková platforma nachází a tak bylo potřeba vytvořit
nějaký systém, který by mohl určit, zda hádanka je nebo není vyřešena.
Systém se zatím skládá z lokací, které určují, jaká platforma v nich má být
a poté aktorem, který kontroluje, že všechny lokace mají v sobě správnou
platformu.
V případě úspěšného vyřešení, může tento aktor ohlásit jiným aktorům (dveře,
NPC), že je vše vyřešeno a můžou hráči uskutečnit další progres.